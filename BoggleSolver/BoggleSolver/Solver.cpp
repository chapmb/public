#include "Solver.h"
#include <assert.h>
#include "Writer.h"

Solver::Solver(DictionaryNode* dictionary, int rows, int columns, const char* board, Writer<char>* writer) :
	_dictionary(dictionary),
	_rows(rows),
	_columns(columns),
	_board(board),
	_dictionaryPath(),
	_visitedSpaces(),
	_wordCount(0),
	_writer(writer) {
	_dictionaryPath.reserve(100);
	_visitedSpaces.reserve(100);
}

Solver::~Solver() {
	_dictionaryPath.clear();
	_visitedSpaces.clear();
	_board = 0;
	_dictionary = 0;
	_writer->Close();
}

void Solver::Solve() {
	int len = _rows * _columns;

	_writer->Open();

	for (int i = 0; i < len; ++i) {
		_dictionaryPath.push_back(_dictionary);
		Visit(i);
		assert(_dictionaryPath.size() == 1);
		_dictionaryPath.pop_back();
	}

	_writer->Flush();
	_writer->Close();
}

void Solver::Visit(int boardIndex) {
	DictionaryNode* node;

	assert(boardIndex >= 0);

	if (IsVisited(boardIndex) || !(node = FindChild(_board[boardIndex]))) {
		return;
	}
	
	_visitedSpaces.push_back(boardIndex);
	_dictionaryPath.push_back(node);
	
	if (node->IsWord()) ReportWord(boardIndex);

	int canLeft = boardIndex % _columns != 0;
	int canRight = (boardIndex + 1) % _columns != 0;
	int canTop = boardIndex >= _columns;
	int canBottom = boardIndex < ((_rows - 1) * _columns);

	if (canLeft) Visit(boardIndex - 1);
	if (canRight) Visit(boardIndex + 1);
	if (canTop) Visit(boardIndex - _columns);
	if (canBottom) Visit(boardIndex + _columns);
	if (canTop && canLeft) Visit(boardIndex - _columns - 1);
	if (canTop && canRight) Visit(boardIndex - _columns + 1);
	if (canBottom && canLeft) Visit(boardIndex + _columns - 1);
	if (canBottom && canRight) Visit(boardIndex + _columns + 1);

	_dictionaryPath.pop_back();
	_visitedSpaces.pop_back();
}

void Solver::ReportWord(int boardIndex) {
	int len = _dictionaryPath.size() - 1;
	DictionaryNode* node = _dictionaryPath[len];

	assert(_dictionaryPath[0] == _dictionary);

	// Words must be > 3 characters
	if (node->_wordCount++ == 0 && len > 2) {
		++_wordCount;

		char* buf = new char[len + 1 + 2];
		for (int i = 0; i < len; ++i) {
			buf[i] = _dictionaryPath[i + 1]->_character;
		}
		buf[len] = '\r';
		buf[len + 1] = '\n';
		buf[len + 2] = 0;
		_writer->Write(buf, 0, len + 1 + 2);
	}
}
