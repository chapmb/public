#pragma once

#include "Reader.h"

class BoardReader
{
public:
	static bool Read(Reader<char>& reader, int rows, int columns, char*& board);
};
