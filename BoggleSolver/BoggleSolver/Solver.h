#pragma once

#include <vector>
#include "Writer.h"
#include "DictionaryNode.h"

using namespace std;

class Solver
{
public:
	Solver(DictionaryNode* dictionary, int rows, int columns, const char* board, Writer<char>* writer);
	~Solver();
	void Solve();
	inline bool IsVisited(int boardIndex) const { return find(_visitedSpaces.begin(), _visitedSpaces.end(), boardIndex) != _visitedSpaces.end(); };
	inline int WordCount() const { return _wordCount; }
private:
	void Visit(int boardIndex);
	DictionaryNode* FindChild(char character) const { return _dictionaryPath[_dictionaryPath.size() - 1]->FindChild(character); }
	void ReportWord(int boardIndex);

	int _rows;
	int _columns;
	int _wordCount;
	const char * _board;
	DictionaryNode* _dictionary;					// Not owned by this class, but modified by this class
	vector<DictionaryNode*> _dictionaryPath;		// Elements are not owned by this class
	vector<int> _visitedSpaces;
	Writer<char>* _writer;							// Not owned by this class
};
