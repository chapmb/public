#pragma once

#include <assert.h>
#include <stdio.h>

template <class T>
class Writer
{
public:
	Writer(int capacity) : _capacity(capacity) {};
	virtual bool Open() = 0;
	virtual bool Write(const T* data, int offset, int count) = 0;
	virtual bool Flush() = 0;
	virtual int Close() = 0;
protected:
	int _capacity;
};

template <class T>
class FileWriter : public Writer<T>
{
public:
	FileWriter(const char* filename, const char* mode, int capacity);
	~FileWriter();
	bool Open();
	bool Write(const T* data, int offset, int count);
	bool Flush();
	int Close();
private:
	FILE* _file;
	const char* _filename;
	const char* _mode;
	char* _buf;
	int _count;
};

template <class T>
FileWriter<T>::FileWriter(const char* filename, const char* mode, int capacity) :
	Writer(capacity),
	_file(0),
	_filename(filename),
	_mode(mode),
	_count(0) {
	_buf = new char[_capacity];
}

template <class T>
FileWriter<T>::~FileWriter()
{
	Close();
	delete[] _buf;
}

template <class T>
bool FileWriter<T>::Open() {
	return !!(_file = fopen(_filename, _mode));
}

template <class T>
bool FileWriter<T>::Write(const T* data, int offset, int count) {
	if (count < 1) return true;
	assert(_count <= _capacity);
	int written = 0;
	while (written != count) {
		while (written < count && _count < _capacity) {
			_buf[_count++] = data[offset + written++];
		}
		assert(_count <= _capacity);
		if (_count == _capacity) {
			if (!Flush()) {
				Close();
				return false;
			}
		}
	}

	return true;
}

template <class T>
bool FileWriter<T>::Flush() {
	if (_count > 0) {
		bool success = (_count == fwrite(_buf, sizeof(char), _count, _file));
		_count = 0;
		return success;
	}
	return true;
}

template <class T>
int FileWriter<T>::Close() {
	if (_file) {
		bool success = Flush();
		success = success && !fclose(_file);
		return success;
	}
	return true;
}
