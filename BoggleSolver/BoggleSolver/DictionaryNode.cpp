#include "DictionaryNode.h"

DictionaryNode::DictionaryNode() :
	_character(0),
	_childrenLen(0),
	_childrenCapacity(0),
	_isWord(0),
	_wordCount(0),
	_children(0) {
}

DictionaryNode::DictionaryNode(int character, int capacity) :
	DictionaryNode() {
	_character = character;
	_childrenCapacity = capacity;
	_children = new DictionaryNode*[_childrenCapacity];
	for (int i = 0; i < _childrenCapacity; ++i) _children[i] = 0;
}

DictionaryNode::~DictionaryNode() {
	for (int i = 0; i < _childrenCapacity; ++i) {
		if (_children[i]) {
			delete _children[i];
			_children[i] = 0;
		}
	}
	delete[] _children;
	_children = 0;
}

DictionaryNode* DictionaryNode::FindChild(char character) const {
	for (int i = 0; i < _childrenLen; ++i) {
		if (_children[i]->_character == character) {
			return _children[i];
		}
	}
	return 0;
}

void DictionaryNode::ShrinkChildren() {
	for (int i = 0; i < _childrenLen; ++i) {
		_children[i]->ShrinkChildren();
	}

	DictionaryNode** buf = new DictionaryNode*[_childrenLen];
	for (int i = 0; i < _childrenLen; ++i) {
		buf[i] = _children[i];
	}
	
	delete[] _children;
	_children = buf;
	_childrenCapacity = _childrenLen;
}