#include "BoardReader.h"
#include <stdio.h>

bool BoardReader::Read(Reader<char>& reader, int rows, int columns, char*& board) {
	int read, len = rows * columns, iBuf, iBoard = 0, capacity = reader.Capacity();
	char* buf = new char[capacity], character;
	
	board = new char[len];
	while ((read = reader.Read(buf, 0, capacity)) > 0) {
		for (iBuf = 0; iBuf < read; ++iBuf) {
			character = buf[iBuf];
			if (character == ',' || character == '\r' || character == '\n') continue;
			board[iBoard++] = character;
		}
	}

	// IO error or bad board
	if (read < 0 || iBoard != len) {
		delete board;
		board = 0;
		return 0;
	}

	return 1;
}
