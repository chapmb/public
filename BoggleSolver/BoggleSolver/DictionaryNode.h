#pragma once

struct DictionaryNode {
public:
	DictionaryNode();
	DictionaryNode(int character, int capacity);
	~DictionaryNode();
	DictionaryNode* FindChild(char character) const;
	inline bool IsWord() const  { return _isWord; };
	void ShrinkChildren();			// Not as useful as I want it to be

	char _character;
	int _childrenLen;
	int _childrenCapacity;
	bool _isWord;
	int _wordCount;					// Hack for performance.  Ideally this object is read only after creation
	DictionaryNode** _children;
};
