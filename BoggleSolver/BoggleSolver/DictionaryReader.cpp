#include "DictionaryReader.h"

#include "DictionaryReader.h"

int DictionaryReader::Read(Reader<char>& reader, DictionaryNode*& dictionary) {
	int read = 0, capacity = reader.Capacity();
	char* buf = new char[capacity];
	DictionaryNode* current;

	dictionary = new DictionaryNode(0, 26);
	current = dictionary;

	while ((read = reader.Read(buf, 0, capacity)) > 0) {
		ProcessChunk(buf, read, dictionary, current);
	}

	delete[] buf;
	reader.Close();

	// Note:  Words cannot have length < 3, this is checked again in the Solver
	if (current != dictionary) {
		current->_isWord = true;
	}

	if (read < 0) {
		delete dictionary;
		dictionary = 0;
		return false;
	}

	return true;
}

void DictionaryReader::ProcessChunk(char* buf, int len, DictionaryNode* dictionary, DictionaryNode*& current) {
	for (int i = 0; i < len; ++i) {
		char character = buf[i];
		if (character != '\r' && character != '\n') {
			DictionaryNode* query = current->FindChild(character);
			if (!query) {
				assert(current->_childrenLen < 26);
				query = new DictionaryNode(character, 26);
				current->_children[current->_childrenLen++] = query;
			}
			current = query;
		}
		else {
			current->_isWord = current != dictionary;
			current = dictionary;
		}
	}
}
