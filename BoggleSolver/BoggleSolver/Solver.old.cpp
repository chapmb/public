//#include <set>
//#include <stack>
//#include <vector>
//#include <algorithm>
//
//using namespace std;
//
//class Path {
//};
//
//
//
//struct FindNodeByCharacter {
//	FindNodeByCharacter(char character)
//};
//
//class DictionaryNode {
//public:
//	
//	//bool operator==(const DictionaryNode& l, const DictionaryNode& r) const {
//	//	return l.id == r.id;
//	//}
//
//	vector<DictionaryNode*>::iterator FindChild(char character) const {
//
//		return find_if(_children.begin(), _children.end(),
//			[&character = character](const DictionaryNode*& node) -> bool { return node->_character == character; });
//
//		//int i = 0;
//		//DictionaryNode* query;
//
//		//for (; i < _count; ++i) {
//		//	query = _children[i];
//		//	if (query->_character == character) {
//		//		return query;
//		//	}
//		//}
//
//		//return 0;
//	}
//private:
//	char _character;
//	int _count;
//	vector<DictionaryNode*> _children;
//};
//
//// TOPLEFT(index = 5, columns = 4)
//// 5 - 4 - 1 = 0 (good)
//// TOPLEFT(index = 4, columns = 4)
//// 4 - 4 - 1 = -1 (good)
//// TOPMIDDLE(I
//
//// 0 1 2 3
//// 4 5 6 7
//// 8 9 0 1
//
//
//#define TOPLEFT(index, columns) (index) - (columns) - 1
//#define TOPCENTER(index, columns) (index) - (columns)
//#define TOPRIGHT(index, columns) (index) - (columns) + 1
//#define LEFT(index, columns) (index) - 1
//#define RIGHT(index, columns) (index) + 1
//#define BOTTOMLEFT(index, columns) (index) + (columns) - 1
//#define BOTTOMCENTER(index, columns) (index) + (columns)
//#define BOTTOMRIGHT(index, columns) (index) + (columns) + 1
//
//void Visit(vector<char>* board, stack<DictionaryNode*>* path, int boardIndex, set<int>* visited, int columns) {
//	DictionaryNode* current, *next;
//
//
//	if (boardIndex < 0 || boardIndex > board->size()) {
//		// The square is outside of the board
//		return;
//	}
//
//	if (visited->find(boardIndex) != visited->end()) {
//		// The board square is already being visited
//		return;
//	}
//
//	current = path->top();
//	next = current->FindChild((*board)[boardIndex]);
//
//	if (!next) {
//		// Not a valid dictionary word
//		return;
//	}
//
//	path->push(next);
//	current = next;
//
//	//// todo
//	//if (current->IsWord()) {
//	//	Add(first, current);
//	//}
//
//	// Visit adjacents
//
//	Visit(board, path, TOPLEFT(boardIndex, columns), visited, columns);
//	Visit(board, path, TOPCENTER(boardIndex, columns), visited, columns);
//	Visit(board, path, TOPRIGHT(boardIndex, columns), visited, columns);
//	Visit(board, path, LEFT(boardIndex, columns), visited, columns);
//	Visit(board, path, RIGHT(boardIndex, columns), visited, columns);
//	Visit(board, path, BOTTOMLEFT(boardIndex, columns), visited, columns);
//	Visit(board, path, BOTTOMCENTER(boardIndex, columns), visited, columns);
//	Visit(board, path, BOTTOMRIGHT(boardIndex, columns), visited, columns);
//}
//
//void Solve(int rows, int columns, vector<char>* board, DictionaryNode* dictionary, stack<DictionaryNode*>* path, set<int>* visited) {
//	int i = 0, j = 0;
//	DictionaryNode* node;
//
//	for (; i < board->size(); ++i) {
//		node = dictionary->FindChild((*board)[i]);
//
//		if (node) {
//			visited->insert(i);
//			path->push(node);
//			Visit(board, path, i, visited, columns);
//			path->pop();
//		}
//	}
//}
