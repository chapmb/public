#include <ctime>
#include <stdio.h>
#include "BoardReader.h"
#include "DictionaryReader.h"
#include "Reader.h"
#include "Solver.h"

using namespace std;

#define READER(File) FileReader<char>((File), "r", 4096)

int SolveBoard(const char* dictionaryFile, int rows, int columns, const char* boardFile, const char* resultFile) {

	printf("Reading dictionary...\r\n");
	char* words = 0;
	DictionaryNode* dictionary = 0;
	if (!DictionaryReader::Read(READER(dictionaryFile), dictionary)) {
		printf("Failed to read dictionary\r\n");
		return 1;
	}

	printf("Shrinking dictionary...\r\n");
	dictionary->ShrinkChildren();

	printf("Reading board...\r\n");
	char* board = 0;
	if (!BoardReader::Read(READER(boardFile), rows, columns, board)) {
		printf("Failed to read board\r\n");
		delete dictionary;
		delete words;
		return 1;
	}

	FileWriter<char> writer(resultFile, "w", 4096);
	Solver solver(dictionary, rows, columns, board, &writer);

	printf("Solving board (rows=%d, columns=%d)...\r\n", rows, columns);
	clock_t start = clock();
	solver.Solve();
	double elapsedSeconds = (clock() - start) / (double)CLOCKS_PER_SEC;

	printf("Solver completed\r\n");
	printf("  Words found:      %d\r\n", solver.WordCount());
	printf("  Elapsed time:  %4.2f seconds\r\n", elapsedSeconds);

	delete dictionary;
	delete[] words;
	delete board;

	return 0;
}

int main(int argc, char** argv) {
	int rows = 100, columns = 100;
	char* boardFile = "..\\Test Files\\Random100x100.csv";
	char* dictionaryFile = "..\\Test Files\\dictionary.txt";
	char* resultFile = "..\\Test Files\\Result100x100.txt";

	//int rows = 2000, columns = 2000;
	//char* boardFile = "..\\Test Files\\Random2000x2000.csv";
	//char* dictionaryFile = "..\\Test Files\\dictionary.txt";
	//char* resultFile = "..\\Test Files\\Result2000x2000.txt";

	if (SolveBoard(dictionaryFile, rows, columns, boardFile, resultFile)) {
		printf("Failed to solve the board\r\n");
		return 1;
	}

	return 0;
}