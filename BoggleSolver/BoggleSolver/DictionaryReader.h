#pragma once

#include "DictionaryNode.h"
#include "Reader.h"

class DictionaryReader
{
public:
	static int Read(Reader<char>& reader, DictionaryNode*& board);
private:
	static void ProcessChunk(char* buf, int len, DictionaryNode* dictionary, DictionaryNode*& current);
};
