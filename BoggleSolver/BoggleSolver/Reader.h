#pragma once

#include <assert.h>
#include <stdio.h>

template <class T>
class Reader {
public:
	virtual int Read(T* buf, int offset, int count) = 0;
	virtual bool Close() = 0;
	inline int Capacity() const { return _capacity; };
protected:
	Reader(int capacity) : _capacity(capacity) { };
	int _capacity;
};

template <class T>
class FileReader : public Reader<T> {
public:
	FileReader(const char* filename, const char* mode, int capacity);
	~FileReader();
	int Read(T* buf, int offset, int count);
	bool Close();
	inline bool IsUnopened() { return !_eof && !_file; }
private:
	const char* _filename;
	const char* _mode;
	FILE* _file;
	bool _eof;
	int _read;
	int _size;
	T* _buf;
};

template <class T>
FileReader<T>::FileReader(const char* filename, const char* mode, int capacity) :
	Reader(capacity),
	_filename(filename),
	_mode(mode),
	_file(0),
	_eof(0),
	_read(0),
	_size(0),
	_buf(0) {
}

template <class T>
FileReader<T>::~FileReader() {
	Close();
	delete[] _buf;
}

// Returns:
//   n > 0:  Success, the number of elements read
//   n == 0:  EOF
//   n < 0:  Error
template <class T>
int FileReader<T>::Read(T* buf, int offset, int count) {
	assert(!(_eof && _file));

	if (IsUnopened()) {
		if (!(_file = fopen(_filename, _mode))) {
			return -1;
		}
		_buf = new char[_capacity];
	}

	int read = 0;
	while (true) {
		while (read < count && _read < _size) {
			buf[offset + read++] = _buf[_read++];
		}
		if (read == count || _eof) return read;
		if ((_size = fread(_buf, sizeof(T), _capacity, _file)) < _capacity) {
			if (!feof(_file) || !Close()) return -1;
		}
		_read = 0;
	}

	return read;
}

template <class T>
bool FileReader<T>::Close() {
	if (!_file) return true;
	bool success = !fclose(_file);
	_file = 0;
	_eof = true;
	return success;
}
